var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');

module.exports = {
    entry: './src/index.js',
    // devtool: 'source-map',
    output: {
        path: './public',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
              test: /\.js$/,
              exclude: /(node_modules|bower_components)/,
              loader: 'babel',
              query: {
                presets: ['es2015']
              }
            },
            {
                test:   /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?minimize!postcss-loader')
            }
        ]
    },
    postcss: function (webpack) {
        return [
            require('postcss-import')({
                addDependencyTo: webpack
            }),
            require('postcss-responsive-type')(),
            require('postcss-cssnext')({
              features: {
                rem: false
              }
            }),
            require('postcss-font-magician')(),
            require('lost')(),
            require('postcss-vertical-rhythm')(),
            require('postcss-autoreset')({
              reset: {
                margin: 0
              }
            }),
        ];
    },
    plugins: [
      new ExtractTextPlugin("styles.css"),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      })
    ]
 };