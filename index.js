var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var fs = require('fs')
var nodemailer = require('nodemailer')

var receivers = 'michelle.walker@landstarmail.com, cayerre@gmail.com, ifa.training@landstarmail.com'
var secret = 'bX0w934$E987'

var transporter = nodemailer.createTransport('smtps://admin%40freightdojo.com:ferrariF12@smtp.zoho.com')

var filePath = __dirname + '/server/data/prospects.csv';

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(express.static('public'))

fs.access(filePath, (err) => {
	if(err) fs.writeFile(filePath, 'Date\tName\tPhone\tEmail\tTime\n');
});

app.post('/join', function(req, res){
	var mailOptions = {
    from: '"Reload Freight Systems 👥" <admin@freightdojo.com>',
    to: receivers,
    subject: '🆕 Training candidate',
    text: `${new Date().toLocaleDateString()}\n${req.body.name}\n${req.body.phone}\n${req.body.email}\n${new Date().toLocaleTimeString()}\n`
	}
	transporter.sendMail(mailOptions, function(error, info){
    if(error){
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);
	});

	fs.appendFile(filePath, `${new Date().toLocaleDateString()}\t${req.body.name}\t${req.body.phone}\t${req.body.email}\t${new Date().toLocaleTimeString()}\n`, 'utf8', (err) => {
		if(err) res.status(500).send();
		res.status(201).json({success: true, data:req.body, created: new Date()}).send()
	});
})

app.get('/admin', function(req, res){
	res.sendFile(__dirname + '/public/admin/index.html')
})

app.post('/admin', function(req, res){
	if(req.body.secret === secret) res.sendFile(filePath)
	else res.redirect('/admin?valid=false')
})

app.listen(process.env.NODE_ENV === 'production' ? 80 : 8080, () => {
	console.log('Server running on 80');
})