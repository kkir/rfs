require('./styles/index.css');
import './js/polyfill';
import './js/cover';
import './js/nav';
import './js/form';
import './js/modal';
import './js/animation';
import './js/faq';
import './js/slider';
import './js/docs';
// import './js/video';

