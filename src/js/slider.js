require.ensure(['lory.js'], function(require) {
  const lory = require('lory.js').lory;
  const slider = document.querySelector('.js_slider');
  lory(slider, {
    infinite: 1
  });
});