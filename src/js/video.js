const instances = plyr.setup();
// Get an element
function get(selector) {
  return document.querySelector(selector);
}

// Custom event handler (just for demo)
function on(element, type, callback) {
  if (!(element instanceof HTMLElement)) {
    element = get(element);
  }
  element.addEventListener(type, callback, false);
}

instances.forEach(function(instance) {
  on('.video-play', 'click', function() { 
    instance.play();
  });
});