import { ready } from './helpers';

ready(() => {
  const formLighbox = document.querySelector('.overlay');

  [...document.querySelectorAll('.docs--link')].map(link => link.addEventListener('click', (e) => {
    e.preventDefault();
    console.log(e.target.hash.slice(1));
    document.body.classList.add('blured');
		formLighbox.style.display = 'block';
    formLighbox.querySelector('form').style.display = 'none';

    var request = new XMLHttpRequest();
    request.open('GET', `/html/${e.target.hash.slice(1)}.html`, true);

    request.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status >= 200 && this.status < 400) {
          // Success!
          if (formLighbox.querySelector('.docs--content')) {
            const c = formLighbox.querySelector('.docs--content');
            console.log(c);
            formLighbox.querySelector('.modal').removeChild(c);
          }
          formLighbox.querySelector('.modal').insertAdjacentHTML('beforeend', this.responseText).scrollTop = 0;
        } else {
          alert('Please try again')
        }
      }
    };

    request.send();
    request = null;

  }))
})

