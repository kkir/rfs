const faq = document.querySelectorAll('.faq li');

Array.prototype.slice.call(faq).forEach(q => q.addEventListener('click', function(e){
  e.preventDefault();
  Array.prototype.filter.call(this.parentNode.children, (child) =>
  child !== this
  ).forEach(li => li.classList.remove('active'));
  this.classList.toggle('active');
}))