require.ensure(['waypoints/lib/noframework.waypoints.js'], function(require) {
	require('waypoints/lib/noframework.waypoints.js');
	
	new Waypoint({
		element: document.querySelector('#bullets-landstar'),
		offset: '75%',
		handler: direction => {
			[...document.querySelectorAll('#bullets-landstar svg')].map(
				(svg, i) => {
					setTimeout(() => {
						svg.classList.add('animated')
					}, i * 500)
				}
			);
		}
	})

	new Waypoint({
		element: document.querySelector('#bullets-rfs'),
		offset: '75%',
		handler: direction => {
			[...document.querySelectorAll('#bullets-rfs svg')].map(
				(svg, i) => {
					setTimeout(() => {
						svg.classList.add('animated')
					}, i * 500)
				}
			);
		}
	})

	// new Waypoint({
	// 	element: document.querySelector('#training'),
	// 	offset: '40%',
	// 	handler: direction => {
	// 		setTimeout(() => {
	// 			document.querySelector('#star').classList.add('animated')
	// 		}, 500)
			
	// 	}
	// })

})