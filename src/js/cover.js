window.onload = () => {
	const cover = document.querySelector('.cover');
  const star = document.querySelector('.star');
	var imgLarge = new Image();
  imgLarge.src = cover.dataset.large; 
  imgLarge.onload = function () {
    imgLarge.classList.add('loaded');
    setInterval(() => {
      const newStar = star.cloneNode();
      newStar.style.left = `${Math.random() * 50}%`;
      cover.appendChild(newStar);
      newStar.classList.add('animated');
      setTimeout(() => {
        newStar.classList.remove('animated');
        newStar.parentNode.removeChild(newStar);
      }, 600);
    }, Math.random() * 5000);
  };
  cover.appendChild(imgLarge);
  // imgLarge.style.transform = `translate3d(0, 0 ,0)`;
  // window.onscroll = () => {
  //   imgLarge.style.transform = `translate3d(0, ${window.pageYOffset * 0.5}px ,0)`
  // }
}
