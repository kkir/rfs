require.ensure([], function(require){
	const Cleave = require('cleave.js');
	require('cleave.js/dist/addons/cleave-phone.us');
	const cleave1 = new Cleave('.phone-lightbox', {
	    phone: true,
	    phoneRegionCode: 'us'
	});
	// const cleave2 = new Cleave('.phone-page', {
	//     phone: true,
	//     phoneRegionCode: 'us'
	// });

	// validation and ajax
	require('script!whatwg-fetch');

	[...document.querySelectorAll('form>button[type="submit"]')].map(submitBtn => submitBtn.addEventListener('click', e => {
		e.preventDefault();
		let values = [...submitBtn.parentElement.querySelectorAll('input')].map(input => {input.style.borderColor = '#000'; return {key: input.name, value: input.value}})
		let errors = [];
		values.forEach(val => {
			switch (val.key){
				case 'phone':
				console.log(val.value, val.value.length)
				val.value.length !== 12 && errors.push({key: 'phone', message: 'required'})
				break
				case 'name':
				!/^[a-zA-Z\s]{2,}$/.test(val.value) && errors.push({key: 'name', message: 'required'})
				break
				case 'email':
				!/(?:[a-z0-9!#$%&'*+/=?^_{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(val.value) && errors.push({key: 'email', message: 'required'})
				break
			}
		});
		console.log(errors)
		if(errors.length === 0) {
			submitBtn.disabled = true
			submitBtn.classList.add('disabled')
			submitBtn.textContent = 'Sending request...'
			
			const data = {};
			values.forEach(val => {data[val.key] = val.value})
			fetch('/join', {
			  method: 'POST',
			  headers: {
			    'Accept': 'application/json',
			    'Content-Type': 'application/json'
			  },
			  body: JSON.stringify(data)
			})
			.then(response => {
				if(response.status !== 201){
					submitBtn.disabled = false
					submitBtn.classList.remove('disabled')
					submitBtn.textContent = 'Request failed. Please try again'
				}
				else return response.json()
			})
			.then(json => {
				if(json.success){
					submitBtn.classList.add('success')
					submitBtn.textContent = 'Thank you! We\'ll call you soon'
				}
				else{
					submitBtn.disabled = false
					submitBtn.classList.remove('disabled')
					submitBtn.textContent = 'Request failed. Please try again'
				}
			})
			.catch(err => console.log(err))
		}
		else {
			submitBtn.classList.add('shake');
			setTimeout(() => submitBtn.classList.remove('shake'), 1000);
			errors.forEach(err => submitBtn.parentElement.querySelector(`input[name="${err.key}"]`).style.borderColor = '#F91B00')
		}
	}))

});