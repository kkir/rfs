

require.ensure([], function(require) {
	const navItems = [...document.querySelectorAll(`header nav a`)];
  require('waypoints/lib/noframework.waypoints.js');
  
  const waypoints = navItems.map((item, i) => new Waypoint({
		  element: document.querySelector(item.hash),
		  offset: 20,
		  handler: function(direction) {
		    
		    navItems.forEach(el => el.classList.remove('active'))
		    if(direction === 'down'){
			    navItems[i].classList.add('active');
		    } else {
		    	i > 0 && navItems[i-1].classList.add('active');
		    }
		  }
		})
  );

  const firstSection = [...document.querySelectorAll('section')][0];
  const header = [...document.querySelectorAll('body > header')][0];
  const reachBottomFirstScreen = new Waypoint({
  	element: firstSection,
  	// offset: header.clientHeight,
  	handler: function(direction) {
  		if (!header.classList.contains('stick')) header.classList.add('stick')
  		else {
  			header.classList.add('unstick')
  			setTimeout(() => {
          header.classList.remove('stick')
  				header.classList.remove('unstick')
  			}, 200)
  		}
  	}
  })

  const jump = require('jump.js');
  let navLinks = [...document.querySelectorAll(`header nav, .btn-nav`)]
  navLinks.map(link => link.addEventListener('click', (e) => {
  		e.preventDefault();
  		jump(e.target.hash, {duration: 300})
  	})
  )
})